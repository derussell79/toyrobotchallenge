﻿using System;
using System.IO;

namespace ToyRobotChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Game game = new Game();

                var lines = File.ReadAllLines("Input1.txt");
                foreach (var rawLine in lines)
                {
                    var line = rawLine.Trim();
                    if (line.Length == 0 || line.StartsWith('#'))
                        continue;
                    if (line.StartsWith("echo "))
                    {
                        Console.WriteLine(line.Remove(0, "echo ".Length));
                        continue;
                    }

                    game.Loop(line);
                }

                Console.WriteLine("---");
                Console.WriteLine("END");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("");
        }
    }
}
