﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ToyRobotChallenge
{
    public enum Commands { Move, Left, Right, Place, Report, Validate };
    public enum Directions { North, East, South, West };
    public enum GameCommandResult { OK, NotPlaced, CannotPlace, CannotMove }

    public class GameCommand
    {
        public Commands? Command { get; set; }

        public int? PlaceX { get; set; }
        public int? PlaceY { get; set; }
        public Directions? PlaceFacing { get; set; }

        public static GameCommand Parse(string text)
        {
            text = text.Trim().ToUpper();
            var splitChars = new[] { ' ', ',' };
            var split = text.Split(splitChars, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length == 0)
                throw new ApplicationException("No command to process.");

            var gameCommand = new GameCommand()
            {
                Command = ParseCommand(split[0])
            };

            switch (gameCommand.Command)
            {
                case Commands.Place:
                case Commands.Validate:
                    if (split.Length < 4)
                        throw new ApplicationException("Too few params for Place");
                    try
                    {
                        gameCommand.PlaceX = ParsePosition(split[1]);
                        gameCommand.PlaceY = ParsePosition(split[2]);
                        gameCommand.PlaceFacing = ParseDirection(split[3]);
                    }
                    catch
                    {
                        throw new ApplicationException("Invalid params for Place");
                    }
                    break;
            }

            return gameCommand;
        }

        private static Commands ParseCommand(string text)
        {
            switch (text)
            {
                case "P": case "PLACE": return Commands.Place;
                case "L": case "LEFT": return Commands.Left;
                case "R": case "RIGHT": return Commands.Right;
                case "M": case "MOVE": return Commands.Move;
                case "REPORT": return Commands.Report;
                case "V": case "VALIDATE": return Commands.Validate;
                default:
                    throw new ApplicationException("Invalid Command: " + text);
            }
        }

        private static int ParsePosition(string text)
        {
            return int.Parse(text);
        }

        private static Directions ParseDirection(string text)
        {
            switch (text)
            {
                case "N": case "NORTH": return Directions.North;
                case "E": case "EAST": return Directions.East;
                case "S": case "SOUTH": return Directions.South;
                case "W": case "WEST": return Directions.West;
                default:
                    throw new ApplicationException("Invalid text for Direction.");
            }
        }

        public override string ToString()
        {
            if (Command.HasValue)
            {
                return $"{Command} {PlaceX} {PlaceY} {PlaceFacing}".Trim();
            }
            return "(No command)";
        }
    }

    public class Robot
    {
        public bool Placed { get; set; }
        public int PosX { get; set; }
        public int PosY { get; set; }
        public Directions Facing { get; set; }

        public override string ToString()
        {
            if (!Placed)
            {
                return "Not placed.";
            }
            return $"{PosX},{PosY},{Facing}";
        }
    }

    public class Game
    {
        int BoardWidth = 5;
        int BoardHeight = 5;

        Robot Robot = new Robot();

        public void Loop(string text)
        {
            try
            {
                GameCommand gameCommand = RetrieveCommand(text);
                Console.Write($"{gameCommand,-20}");

                GameCommandResult result = ExecuteCommand(gameCommand);
                Console.WriteLine($"{ result,10 }     - { Robot }");
            }
            catch (ApplicationException ex)
            {
                Console.WriteLine("Application Exception: " + ex.Message ?? "");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message ?? "");
            }

        }
        public GameCommand RetrieveCommand(string text)
        {
            return GameCommand.Parse(text);
        }

        private GameCommandResult ExecuteCommand(GameCommand gameCommand)
        {
            // Todo: Abstract commands into separate classes
            switch (gameCommand.Command)
            {
                case Commands.Place:
                    // Do we allow re-placement?
                    return ExecutePlace(gameCommand);
                case Commands.Left:
                case Commands.Right:
                    return ExecuteTurn(gameCommand);
                case Commands.Move:
                    return ExecuteMove(gameCommand);
                case Commands.Report:
                    return ExecuteReport(gameCommand);
                case Commands.Validate:
                    return ExecuteValidate(gameCommand);
            }

            return GameCommandResult.OK;
        }

        private GameCommandResult ExecutePlace(GameCommand gameCommand)
        {
            if (!gameCommand.PlaceX.HasValue || gameCommand.PlaceX < 0 || gameCommand.PlaceX >= BoardWidth)
                throw new ApplicationException("Invalid Placement X");
            if (!gameCommand.PlaceY.HasValue || gameCommand.PlaceY < 0 || gameCommand.PlaceY >= BoardHeight)
                throw new ApplicationException("Invalid Placement Y");
            if (!gameCommand.PlaceFacing.HasValue)
                throw new ApplicationException("Invalid Placement Facing");

            Robot.Placed = true;
            Robot.PosX = gameCommand.PlaceX.Value;
            Robot.PosY = gameCommand.PlaceY.Value;
            Robot.Facing = gameCommand.PlaceFacing.Value;

            return GameCommandResult.OK;
        }

        private GameCommandResult ExecuteTurn(GameCommand gameCommand)
        {
            int direction = (int)Robot.Facing;
            switch (gameCommand.Command)
            {
                case Commands.Left: direction--; break;
                case Commands.Right: direction++; break;
                default: throw new ApplicationException("Invalid command for Turn.");
            }

            Robot.Facing = (Directions)((direction+4) % 4);

            return GameCommandResult.OK;
        }

        private GameCommandResult ExecuteMove(GameCommand gameCommand)
        {
            if (!Robot.Placed)
                return GameCommandResult.NotPlaced;

            int newX = Robot.PosX;
            int newY = Robot.PosY;

            switch (Robot.Facing)
            {
                case Directions.North: newY++; break;
                case Directions.East: newX++; break;
                case Directions.South: newY--; break;
                case Directions.West: newX--; break;
            }

            if (CheckValidPosition(newX, newY))
            {
                Robot.PosX = newX;
                Robot.PosY = newY;
                return GameCommandResult.OK;
            }

            return GameCommandResult.CannotMove;
        }


        private GameCommandResult ExecuteReport(GameCommand gameCommand)
        {
            // TODO: Inject an output target
            Console.Write(Robot.ToString());
            return GameCommandResult.OK;
        }

        private GameCommandResult ExecuteValidate(GameCommand gameCommand)
        {
            if (!Robot.Placed)
                return GameCommandResult.NotPlaced;
            if (gameCommand.PlaceX == Robot.PosX &&
                gameCommand.PlaceY == Robot.PosY &&
                gameCommand.PlaceFacing == Robot.Facing
                )
                return GameCommandResult.OK;

            return GameCommandResult.CannotPlace;
        }


        private bool CheckValidPosition(int newX, int newY)
        {
            if (newX < 0 || newX >= BoardWidth)
                return false;
            if (newY < 0 || newY >= BoardHeight)
                return false;
            return true;
        }
    }

}
